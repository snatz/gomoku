package gomoku.core;

import gomoku.core.model.Spot;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

@Aspect
public class LogMoveAspect {
    private String logFile = "moves.log";
    private Path logFilePath = Paths.get(logFile);

    @Around("execution(* *.makeMove(..))")
    public Object LogPlayerMove(final ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = joinPoint.proceed();

        Spot spot = (Spot) joinPoint.getArgs()[0];
        List<String> line = new ArrayList<>();
        line.add("Played at (" + spot.x + "," + spot.y + ") as " + spot.getOccupant().getColor() + "\n");

        if (logFilePath.toFile().exists()) {
            Files.write(logFilePath, line, Charset.defaultCharset(), StandardOpenOption.APPEND);
        } else {
            Files.write(logFilePath, line, Charset.defaultCharset(), StandardOpenOption.CREATE);
        }

        return result;
    }
}
