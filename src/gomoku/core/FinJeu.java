package gomoku.core;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;


@Aspect
public class FinJeu {
    private boolean gameEnded = false;
    private int round = 0;

    @Around("execution(* *.makeMove(..))")
    public Object preventMove(final ProceedingJoinPoint joinPoint) throws Throwable{
        if(gameEnded)
            return null;
        round++;
        return joinPoint.proceed();
    }
    @Around("execution(* *.gameOver(..))")
    public Object detectWin(final ProceedingJoinPoint joinPoint) throws Throwable{
        if(!gameEnded)
            System.out.println("After " + (int)(round/2) + " rounds, " + ((Player)joinPoint.getArgs()[0]).getName() + " wins");
        gameEnded = true;
        return null;
    }
}
