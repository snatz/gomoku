package gomoku.core;

import javafx.scene.paint.Color;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class AdversaireJ {
    private boolean peerTurn = false;
    private Player oddPlayer = new Player("Mr Blue", Color.BLUEVIOLET);
    private Player peerPlayer = new Player("Mr Red", Color.ORANGERED);

    @Around("execution(* *.getCurrentPlayer())")
    public Object ChangePlayer(final ProceedingJoinPoint joinPoint) throws Throwable{
        peerTurn = !peerTurn;
        if(!peerTurn)
            return oddPlayer;
        return peerPlayer;
    }
}
